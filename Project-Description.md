
# Nuts About Candies

**Maintained by:** Ms. Charo Nuguid <me@thegeekettespeaketh.com>

## Introduction

This document describes a shopping cart application.

## Features

1. Inventory
	1. Add/update/remove item
	1. Add/update/remove category (All about nuts, All about candies, Nuts and candies)
	1. Add/update/remove product (All about nuts, King of Nuts, etc.)
	1. Show all categories
	1. Show all products/Show all products by category
	1. Show all items/Show all items by category/Show all items by product
1. Shopping Cart
	1. Get item
	1. Remove item
	1. Update item
	1. Show/hide shopping cart (optional)
1. Cashier
	1. Checkout
	1. Record purchases
1. Login
	1. Admin and regular user login
	1. Shopping cart saving
	1. Purchase history
1. Reports

## Description

Jen's business is called "Nuts About Candies". Her store buys imported nuts and candies in bulk, repackages them and then sells them in smaller packages. The products are packaged and priced as follows.

<table>
<thead>
  <tr>
    <th class="tg-82fe">Product Type</th>
    <th class="tg-82fe">Small<br>(50 grams)</th>
    <th class="tg-82fe">Medium<br>(100 grams)</th>
    <th class="tg-82fe">Large<br>(150 grams)</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky" rowspan="4">All About Nuts</td>
    <td class="tg-34fe" colspan="3"><center>Premium</center></td>
  </tr>
  <tr>
    <td class="tg-c3ow">60.00</td>
    <td class="tg-c3ow">110.00</td>
    <td class="tg-c3ow">160.00</td>
  </tr>
  <tr>
    <td class="tg-34fe" colspan="3"><center>Regular</center></td>
  </tr>
  <tr>
    <td class="tg-c3ow">40.00</td>
    <td class="tg-c3ow">90.00</td>
    <td class="tg-c3ow">140.00</td>
  </tr>
  <tr>
    <td class="tg-y698" rowspan="4">All About Candies</td>
    <td class="tg-34fe" colspan="3"><center>Premium</center></td>
  </tr>
  <tr>
    <td class="tg-yj5y">70.00</td>
    <td class="tg-yj5y">120.00</td>
    <td class="tg-yj5y">170.00</td>
  </tr>
  <tr>
    <td class="tg-34fe" colspan="3"><center>Regular</center></td>
  </tr>
  <tr>
    <td class="tg-yj5y">50.00</td>
    <td class="tg-yj5y">100.00</td>
    <td class="tg-yj5y">150.00</td>
  </tr>
  <tr>
    <td class="tg-0pky" rowspan="4">Nuts and Candies</td>
    <td class="tg-34fe" colspan="3"><center>Premium</center></td>
  </tr>
  <tr>
    <td class="tg-c3ow">80.00</td>
    <td class="tg-c3ow">130.00</td>
    <td class="tg-c3ow">180.00</td>
  </tr>
  <tr>
    <td class="tg-34fe" colspan="3"><center>Regular</center></td>
  </tr>
  <tr>
    <td class="tg-c3ow">60.00</td>
    <td class="tg-c3ow">110.00</td>
    <td class="tg-c3ow">160.00</td>
  </tr>
</tbody>
</table>

The following products are available.

- All About Nuts (premium)
	- Kings of Nuts (Macadamia, Pistachios and Cashews)
	- Mixed Nut Brittle (Peanuts, almonds and cashews covered in a sweet and salty caramel brittle)
	- Cheesy Mac (Parmesan Cheese Macadamia Nuts)
- All About Nuts (regular) 
	- Bar Nuts (Spicy sweet peanuts, almonds, walnuts, brazil nuts)
	- Crazy Samurai (Japanese rice crackers, soy nuts and wasabi peas)
	- Cathay (batter-coated peanuts and soy nuts, barbecued almonds, sesame sticks, pumpkin seeds and roasted peanuts)
	- Garlic Peanuts
- All About Candies (premium)
	- Salt water taffy
	- Caramels
	- English Toffee
	- Nougat Whirls (Caramel, peanut and chocolate nougat swirls)
- All About Candies (regular)
	- Sour Balls
	- Jelly Beans 
	- Jumbo Jellies (Rainbow colored gumdrops)
- Nuts and Candies (premium)
	- Fruit and Nut (Roasted almonds with Chocolate Raisins)
	- Rave Dark (Roasted almonds, cashews, and hazel nuts covered in dark belgian chocolate)
	- Rave Milk (Roasted almonds, cashews, and hazel nuts covered in belgian  milk chocolate)
	- Almond Roca
- Nuts and Candies (regular)
	- Rainbow Peanuts (Peanut wrapped in chocolate in a crunchy candy shell)
	- Tropical (Pineapple chunks, papaya wedges, coconut, sweet banana slices, brazil nuts and rich toasted cashews)

Customers can select the products they want to purchase including the sizes and quantities. Jen calculates the total purchase, reminds the customer of the shelf life and has the purchase approved by the customer. Once approved, she gets their name and address and then ships the purchase.

## Notes

- The minimum delivery fee is 50 pesos. Anything over 500 grams will be charged an additional 10 pesos per 100 grams.
- Jen offers a discount of 5% for any purchase greater than or equal to 2000 pesos. This is computed before the delivery charge.
- Jen gives free delivery for any purchase greater than or equal to 5000 pesos.
- All products with nuts have a maximum shelf life of 1 month (30 days) when kept in a cool dry place.
- All products with just candies have a maximum shelf life of 3 months (90 days) when kept in a cool dry place.
- At the end of every month, Jen wants a report of the total profit, a report that shows the profit margin in percentage for each product and a note if the profit margin is less than the minimum allowed (25%).
- Jen does not make a profit out of the delivery charge.
- Jen makes 1kg batches of each product. She is notified via email and on the inventory section when a product is sold out so she can replenish the stock with a new batch.
- If remaining stock has expired, stock will go to zero. Jen will be notified that stock has expired. Expired stock and corresponding loss should show up on the monthly report.

